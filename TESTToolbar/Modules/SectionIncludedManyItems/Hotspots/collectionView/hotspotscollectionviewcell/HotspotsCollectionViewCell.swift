//
//  HotspotsCollectionViewCell.swift
//  TESTToolbar
//
//  Created by Islam Abotaleb on 12/22/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit

class HotspotsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var hotspotsStackView: UIStackView!
    @IBOutlet weak var subtitlenamehotspotlbl: UILabel!
    @IBOutlet weak var titlenamehotspotslbl: UILabel!
    @IBOutlet weak var hotspotimageview: UIImageView!
    
}
