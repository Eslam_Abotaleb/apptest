//
//  EventsCollectionViewCell.swift
//  TESTToolbar
//
//  Created by Islam Abotaleb on 12/22/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit

class EventsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var eventsStackView: UIStackView!
    @IBOutlet weak var subtitlenameeventslbl: UILabel!
    @IBOutlet weak var titlenameeventslbl: UILabel!
    @IBOutlet weak var eventimageview: UIImageView!
    
}
