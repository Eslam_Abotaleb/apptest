//
//  AttractionsCollectionViewCell.swift
//  TESTToolbar
//
//  Created by Islam Abotaleb on 12/22/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit

class AttractionsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var attractionsStackView: UIStackView!
    @IBOutlet weak var subtitlenameattractionslbl: UILabel!
    @IBOutlet weak var titlenameattractionslbl: UILabel!
    @IBOutlet weak var attractionimageview: UIImageView!
}
