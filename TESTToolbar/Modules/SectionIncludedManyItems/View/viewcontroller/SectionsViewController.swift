//
//  SectionsViewController.swift
//  TESTToolbar
//
//  Created by Islam Abotaleb on 12/22/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class SectionsViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate {
    
    
    
    @IBOutlet weak var attractionsCollectionView: UICollectionView!
    @IBOutlet weak var eventsCollectionView: UICollectionView!
    @IBOutlet weak var hotspotsCollectionView: UICollectionView!
    
    
    
    var namesHotspotsArray = [String]()
    var descriptionHotspotsArray = [String]()
    var imagesHotspots = [String]()
    
    var nameEventsArray = [String]()
    var descriptionEventsArray = [String]()
    var imagesEvents = [String]()
    
    
    var nameAttractionsArray = [String]()
    var descriptionAttractionsArray = [String]()
    var imagesAttractions = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        hotspotsCollectionView.delegate = self
        hotspotsCollectionView.dataSource = self
        eventsCollectionView.delegate = self
        eventsCollectionView.dataSource = self
        attractionsCollectionView.delegate = self
        attractionsCollectionView.dataSource = self
        getHotspotsDataFromFirestore()
        getEventsDataFromFireStore()
        getAttractionsDataFromFireStore()
    }
    
    func getHotspotsDataFromFirestore() {
        
        let firestoreDatabase = Firestore.firestore()
        firestoreDatabase.collection("Hotspots").addSnapshotListener { (snapShot, error) in
            if error != nil {
                print(error?.localizedDescription ?? "Error")
            } else {
                //Retrieve Data
                if snapShot?.isEmpty != true  && snapShot != nil {
                    self.namesHotspotsArray.removeAll(keepingCapacity: false)
                    self.imagesHotspots.removeAll(keepingCapacity: false)
                    self.descriptionHotspotsArray.removeAll(keepingCapacity: false)
                    for document in  snapShot!.documents {
                        
                        if let namehotspots =  document.get("nameHotspots") as? String {
                            self.namesHotspotsArray.append(namehotspots)
                        }
                        if let descriptionhotspots =  document.get("descriptionHotspots") as? String {
                            self.descriptionHotspotsArray.append(descriptionhotspots)
                        }
                     
                        if let imageUrl = document.get("imageUrl") as? String {
                            
                            self.imagesHotspots.append(imageUrl)
                        } else {
                            print("nil")
                        }
                    }
                    self.hotspotsCollectionView.reloadData()
                    
                }
            }
        }
    }
    
    func getEventsDataFromFireStore() {
        let firestoreDatabase = Firestore.firestore()
          firestoreDatabase.collection("Events").addSnapshotListener { (snapShot, error) in
              if error != nil {
                  print(error?.localizedDescription ?? "Error")
              } else {
                  //Retrieve Data
                  if snapShot?.isEmpty != true  && snapShot != nil {
                      self.nameEventsArray.removeAll(keepingCapacity: false)
                      self.imagesEvents.removeAll(keepingCapacity: false)
                      self.descriptionEventsArray.removeAll(keepingCapacity: false)
                      for document in  snapShot!.documents {
                          if let nameEvents =  document.get("nameEvents") as? String {
                              self.nameEventsArray.append(nameEvents)
                          }
                          if let descriptionEvents =  document.get("descriptionEvent") as? String {
                              self.descriptionEventsArray.append(descriptionEvents)
                          }
                       
                          if let imageUrl = document.get("imageUrl") as? String {
                              self.imagesEvents.append(imageUrl)
                          } else {
                              print("nil")
                          }
                      }
                      self.eventsCollectionView.reloadData()
                      
                  }
              }
          }
    }

    func getAttractionsDataFromFireStore() {
        let firestoreDatabase = Firestore.firestore()
          firestoreDatabase.collection("Attractions").addSnapshotListener { (snapShot, error) in
              if error != nil {
                  print(error?.localizedDescription ?? "Error")
              } else {
                  //Retrieve Data
                  if snapShot?.isEmpty != true  && snapShot != nil {
                      self.nameAttractionsArray.removeAll(keepingCapacity: false)
                      self.imagesAttractions.removeAll(keepingCapacity: false)
                      self.descriptionAttractionsArray.removeAll(keepingCapacity: false)
                      for document in  snapShot!.documents {
                          if let nameAttractions =  document.get("nameAttraction") as? String {
                              self.nameAttractionsArray.append(nameAttractions)
                          }
                          if let descriptionAttractions =  document.get("descriptionAttraction") as? String {
                              self.descriptionAttractionsArray.append(descriptionAttractions)
                          }
                       
                          if let imageUrl = document.get("imageUrl") as? String {
                              self.imagesAttractions.append(imageUrl)
                          } else {
                              print("nil")
                          }
                      }
                      self.attractionsCollectionView.reloadData()
                      
                  }
              }
          }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == hotspotsCollectionView {
            return namesHotspotsArray.count
        } else if collectionView == eventsCollectionView {
            return nameEventsArray.count
        } else  if collectionView == attractionsCollectionView {
             return nameAttractionsArray.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == hotspotsCollectionView {
            let cell = hotspotsCollectionView.dequeueReusableCell(withReuseIdentifier: "hotspotsId", for: indexPath) as? HotspotsCollectionViewCell
                  cell?.titlenamehotspotslbl.text = namesHotspotsArray[indexPath.row]
                  cell?.subtitlenamehotspotlbl.text = descriptionHotspotsArray[indexPath.row]
            if self.imagesHotspots[indexPath.row] == "" {
                cell?.hotspotimageview.sd_setImage(with: URL(string: "https://www.findlight.net/front-media/products/images/listing-thumb/__findlight_noimage.jpg"))
            } else {
                cell?.hotspotimageview.sd_setImage(with: URL(string: self.imagesHotspots[indexPath.row]))

            }
                  return cell!
   
        } else  if collectionView == eventsCollectionView {
                   let cell = eventsCollectionView.dequeueReusableCell(withReuseIdentifier: "eventscellid", for: indexPath) as? EventsCollectionViewCell
                         cell?.titlenameeventslbl.text = nameEventsArray[indexPath.row]
                         cell?.subtitlenameeventslbl.text = descriptionEventsArray[indexPath.row]
                   if self.imagesEvents[indexPath.row] == "" {
                    
                    cell?.eventimageview.sd_setImage(with: URL(string: "https://www.findlight.net/front-media/products/images/listing-thumb/__findlight_noimage.jpg"))
                   } else {
                       cell?.eventimageview.sd_setImage(with: URL(string: self.imagesEvents[indexPath.row]))

                   }
                         return cell!
            
               }  else if collectionView == attractionsCollectionView {
            let cell = attractionsCollectionView.dequeueReusableCell(withReuseIdentifier: "attractioncellid", for: indexPath) as? AttractionsCollectionViewCell
                  cell?.titlenameattractionslbl.text = nameAttractionsArray[indexPath.row]
                  cell?.subtitlenameattractionslbl.text = descriptionAttractionsArray[indexPath.row]
            if self.imagesAttractions[indexPath.row] == "" {
             cell?.attractionimageview.sd_setImage(with: URL(string: "https://www.findlight.net/front-media/products/images/listing-thumb/__findlight_noimage.jpg"))
            } else {
                cell?.attractionimageview.sd_setImage(with: URL(string: self.imagesAttractions[indexPath.row]))
            }
                  return cell!
        } else {
            return UICollectionViewCell()
        }
      
    }
}
