//
//  RoundUIView.swift
//  TESTToolbar
//
//  Created by Islam Abotaleb on 12/20/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit
@IBDesignable
class RoundUIView: UIView {
    
    @IBInspectable var borderColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 3.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 38.5 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var backgroundView: UIView? {
       
        didSet {
            backgroundView!.backgroundColor = UIColor(patternImage: UIImage(named: "rectangle67.png")!)
        }
    }
}
